@extends('layouts.app')
@section('title', 'Category')
@section('content')
<div class="row mb-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <a href="{{ url('category/create') }}" class="btn btn-primary mb-4"><i class="fa fa-plus"></i> Add Data</a>
                <div class="table-responsive">
                    <table class="table center-aligned-table">
                        <thead>
                            <tr class="text-primary">
                                <th>No</th>
                                <th>Category</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (\App\Category::all() as $category)
                            <tr>
                                <td>1</td>
                                <td>{{ $category->nama }}</td>
                                <td>{{ $category->slug }}</td>
                                <td>
                                    <a href="{{ url('category/' . $category->id . '/edit') }}" class="btn btn-primary btn-sm">Edit</a>
                                    <a href="{{ url('category/' . $category->id) }}" class="btn btn-danger btn-sm btn-remove">Remove</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
    $('.btn-remove').click(function (e) {
        e.preventDefault();

        var tr = $(this).parent().parent();

        var url = $(this).attr('href');

        $('#modalRemove').modal('show');

        $('#modalRemove .btn-confirm').click(function () {
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    _method: 'DELETE',
                }
            }).done(function (response) {
                if (response) {
                    tr.slideUp();
                    $('#modalRemove').modal('hide');
                }
            });
        });
    });
</script>
@endpush