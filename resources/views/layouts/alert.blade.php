@if (session('error'))
<div class="alert alert-danger">
	<p>{{ session('error') }}</p>
</div>
@endif
@if (session('success'))
<div class="alert alert-success">
	<p>{{ session('success') }}</p>
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger">
	@foreach ($errors->all() as $error)
	<p>{{ $error }}</p>
	@endforeach
</div>
@endif
<div class="modal" tabindex="-1" role="dialog" id="modalRemove">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<p>Are you sure?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-confirm">Yes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>